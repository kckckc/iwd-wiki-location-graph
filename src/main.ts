import ForceGraph from 'force-graph';
import './style.css';

// IWD
const QUERY =
	'[[Located in::+]]|?Located in|format=json|offset=OFFSETHERE|limit=500';
const queryURL = `https://wiki.isleward.com/api.php?action=ask&origin=*&format=json&limit=500&query=${encodeURIComponent(
	QUERY
)}&api_version=2`;

// GW2
// const QUERY =
// 	'[[Located in::+]][[Has context::Location]]|?Located in|format=json|offset=OFFSETHERE|limit=500';
// const queryURL = `https://wiki.guildwars2.com/api.php?action=ask&origin=*&format=json&limit=500&query=${encodeURIComponent(
// 	QUERY
// )}&api_version=2`;

let nodes: any[] = [];
let edges: any[] = [];

const max = 100000;
const sleep = 1000;

const getNext = async (offset: number): Promise<any> => {
	const qurl = queryURL.replace('OFFSETHERE', '' + offset);

	const raw = await (await fetch(qurl)).json();

	const results = Object.values(raw?.query?.results ?? {});

	console.log(offset + ' / ' + max);

	results.forEach((r: any) => {
		const current = r.fulltext;

		if (current.includes('#')) return;

		nodes.push({
			id: current,
			name: current,
		});

		r.printouts['Located in']
			.map((l: any) => l.fulltext)
			.forEach((parent: string) => {
				edges.push({
					source: parent,
					target: current,
				});
			});
	});

	if (!raw['query-continue-offset']) {
		console.log('DONE');
		return;
	}

	if (offset >= max) return;

	await new Promise((resolve) => setTimeout(resolve, sleep));

	return await getNext(raw['query-continue-offset']);
};

const main = async () => {
	await getNext(0);

	// Safety
	// const ensureExists = (e: any, x: string) => {
	// 	if (!nodes.find((n) => n.id === x)) {
	// 		nodes.push({
	// 			id: x,
	// 			name: x,
	// 		});
	// 	}
	// };
	// edges.forEach((e) => {
	// 	ensureExists(e, e.source);
	// 	ensureExists(e, e.target);
	// });

	edges = edges.filter((e) => {
		if (!nodes.find((n) => n.id === e.source)) {
			return false;
		}
		if (!nodes.find((n) => n.id === e.target)) {
			return false;
		}
		return true;
	});

	const data = {
		nodes,
		links: edges,
	};

	const container = document.getElementById('app')!;

	const mygraph = ForceGraph();
	mygraph(container)
		.graphData(data)
		.nodeCanvasObject((node, ctx, globalScale) => {
			const label = node.id as string;
			const fontSize = 12 / globalScale;
			ctx.font = `${fontSize}px Sans-Serif`;
			const textWidth = ctx.measureText(label).width;
			const bckgDimensions: [number, number] = [textWidth, fontSize].map(
				(n) => n + fontSize * 0.2
			) as [number, number]; // some padding

			ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
			ctx.fillRect(
				node.x! - bckgDimensions[0] / 2,
				node.y! - bckgDimensions[1] / 2,
				...bckgDimensions
			);

			ctx.textAlign = 'center';
			ctx.textBaseline = 'middle';
			ctx.fillStyle = '#20B2AA'; //node.color;
			ctx.fillText(label, node.x!, node.y!);

			// @ts-ignore
			node.__bckgDimensions = bckgDimensions; // to re-use in nodePointerAreaPaint
		})
		.nodePointerAreaPaint((node, color, ctx) => {
			ctx.fillStyle = color;
			// @ts-ignore
			const bckgDimensions = node.__bckgDimensions;
			bckgDimensions &&
				ctx.fillRect(
					node.x! - bckgDimensions[0] / 2,
					node.y! - bckgDimensions[1] / 2,
					...(bckgDimensions as [number, number])
				);
		});
	// .dagMode('radialin');
};

main();
